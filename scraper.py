from datetime import datetime
import asyncio
import aiohttp
import json
from colorlog import ColoredFormatter
import logging

class Logger_config:
	def __init__(self):
		self.formatter = ColoredFormatter(
			"%(log_color)s%(levelname)s: %(log_color)s%(asctime)s: %(blue)s%(message)-10s  ",
			datefmt=None,
			reset=True,
			log_colors={
				'DEBUG':    'cyan',
				'INFO':     'cyan',
				'WARNING':  'yellow',
				'ERROR':    'red',
				'CRITICAL': 'red',
			},
			style='%'
		)
		self.handler = logging.StreamHandler()
		self.handler.setFormatter(self.formatter)
		self.logger = logging.getLogger('example')
		self.logger.setLevel(logging.INFO)
		self.logger.addHandler(self.handler)
	def get_logger(self):
		return self.logger

logger = Logger_config().get_logger()

async def critical (text):
    logger.critical(text)

class Scraper:
    def __init__(self):
        self.headers = {
                      'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:123.0) Gecko/20100101 Firefox/123.0',
                      'Accept': 'application/vnd.api+json',
                      'Accept-Language': 'en-US,en;q=0.5',
                      'Accept-Encoding': 'gzip, deflate, br',
                      'Referer': 'https://www.realestate.co.nz/',
                      'Origin': 'https://www.realestate.co.nz',
                      'Connection': 'keep-alive',
                      'Sec-Fetch-Dest': 'empty',
                      'Sec-Fetch-Mode': 'cors',
                      'Sec-Fetch-Site': 'same-site',
                      'TE': 'trailers'}
        self.timestamp = datetime.utcnow()
        self.full_key_list = ['estimated-value', 'bedrooms-total-count', 'land-area', 'website-full-url', 'parking-garage-count', 'parking-other-count', 'photo-primary', 'estimated-values', 'street-address', 'floor-area-unit', 'deferred-share-url', 'council-evaluations', 'category-search', 'short-id', 'bathrooms-total-count', 'address-slug', 'council-information', 'bathroom-ensuite-count', 'raf-id', 'sales-history', 'bathroom-wc-count', 'storey-count', 'address', 'suburb-name', 'floor-area', 'nearby-recent-sales', 'latest-sale-listing', 'land-area-unit', 'has-swimming-pool', 'url', 'latest-rent-listing', 'current-listings', 'parking-covered-count', 'building-age', 'latest-sale', 'website-slug', 'category', 'matched-listings']

    async def info(self, text):
        logger.info(text)
    async def critical(self, text):
        logger.critical(text)

    def write_to_json(self, data, file_name):
        with open(file_name, 'a', encoding='utf-8') as file:
            for document in data:
                json.dump(document, file)
                file.write('\n')

    async def single_scrape(self, base, groupby, sem, base_url):
            parameter = {
                # "filter[saleDateMin]": "2023-02-20",
                "page[limit]": 300,
                "page[groupBy]" : groupby,
                "page[offset]" : base
            }
            request_holder = []
            async with sem:
                await asyncio.sleep(0.2)
                async with aiohttp.ClientSession() as session:
                    try:
                        async with session.get(url= base_url, headers=self.headers, params=parameter) as r:
                            if r:
                                try:
                                    json_ = await r.json()
                                    data = json_["data"]
                                    for each in data:
                                        document = each["attributes"]
                                        request_holder.append(document)
                                    await asyncio.to_thread(self.write_to_json, request_holder, "C:/Projects/Web projects/Database/Text/Realestate.co ML/All.json")
                                    await self.info(base)
                                except Exception as e :
                                    await self.critical(f"error:{e}")
                                    return
                            else:
                              await self.critical(r)
                    except Exception as e:
                        await self.critical(e)
                        return

    async def gather(self):
        tasks = []
        sem = asyncio.Semaphore(10)
        for base_url in ["https://platform.realestate.co.nz/search/v1/properties?filter[saleDateMin]=2023-03-11&filter[propertyTypes][]=Residence",
                         "https://platform.realestate.co.nz/search/v1/properties?filter[saleDateMin]=2023-03-11&filter[propertyTypes][]=Townhouse",
                         "https://platform.realestate.co.nz/search/v1/properties?filter[saleDateMin]=2023-03-11&filter[propertyTypes][]=Unit",
                         "https://platform.realestate.co.nz/search/v1/properties?filter[saleDateMin]=2023-03-11&filter[propertyTypes][]=Home and Income",
                         "https://platform.realestate.co.nz/search/v1/properties?filter[saleDateMin]=2023-03-11&filter[propertyTypes][]=Apartment&",
                         "https://platform.realestate.co.nz/search/v1/properties?filter[saleDateMin]=2023-03-11&filter[propertyTypes][]=Residential Section",
                         "https://platform.realestate.co.nz/search/v1/properties?filter[saleDateMin]=2023-03-11&filter[propertyTypes][]=Residential Investment Block",
                         "https://platform.realestate.co.nz/search/v1/properties?filter[saleDateMin]=2023-03-11&filter[propertyTypes][]=Conversion",
                         "https://platform.realestate.co.nz/search/v1/properties?filter[saleDateMin]=2023-03-11&filter[propertyTypes][]=Lifestyle Blocks&meta"
                                             ]:
            for groupby in ["oldest-sale", "latest-sale","lowest-sold-price", "highest-sold-price"]:
                for base in range(0,10000,300):
                    tasks.append(self.single_scrape(sem=sem, base=base, groupby=groupby, base_url=base_url))
        await asyncio.gather(*tasks)


scraper = Scraper()
loop = asyncio.get_event_loop()
loop.run_until_complete(scraper.gather())








