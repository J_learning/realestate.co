import json
import csv
import numpy as np


with open("C:/Projects/Web projects/Database/Text/Realestate.co ML/residence.json", "r") as file:
    dataset = json.load(file)

dataframe = []
for document in dataset:
    building_age = document["building-age"]
    bedroom = document["bedrooms-total-count"]
    land = document["land-area"]
    url = document["website-full-url"]
    latest_sale_price = float(document["latest-sale"]['sale-price-display'].replace("$","").replace(",",""))
    latest_sale_method = document["latest-sale"]['sale-method']
    latest_sale_date = document["latest-sale"]['sale-date']
    latest_sold_days = document["latest-sale"]['sell-days']
    latest_sold_category = document["latest-sale"]['category']
    parking_primary = document["parking-garage-count"]
    parking_other = document["parking-other-count"]
    bathroom_ensuite = document["bathroom-ensuite-count"]
    bathroom_wc = document["bathroom-wc-count"]
    floor_area = document["floor-area"]
    swim_pool = document["has-swimming-pool"]
    latitude = document["address"]['latitude']
    longitude = document["address"]['longitude']
    postcode = document["address"]['postcode']
    full_address = document["address"]['full-address']
    region = document["address"]['region']
    district = document["address"]['district']
    suburb = document["address"]['suburb']
    street = document["address"]['street-address']

    row = [building_age, latest_sold_category, latest_sale_price, bedroom, land, url,  latest_sale_method, latest_sale_date, latest_sold_days, parking_primary, parking_other, bathroom_ensuite, bathroom_wc, floor_area, swim_pool, latitude, longitude, postcode, full_address, region, district, suburb, street]
    dataframe.append(row)

with open("C:/Projects/Web projects/Database/Text/Realestate.co ML/residence.csv", "w") as file:
    csv_writer = csv.writer(file)
    csv_writer.writerow(["building_age", "latest_sold_category", "latest_sale_price", "bedroom", "land", "url", "latest_sale_method",
           "latest_sale_date", "latest_sold_days", "parking_primary", "parking_other", "bathroom_ensuite",
           "bathroom_wc", "floor_area", "swim_pool", "latitude", "longitude", "postcode", "full_address",
           "region", "district", "suburb", "street"])
    csv_writer.writerows(dataframe)
