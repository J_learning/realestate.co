# Realestate.co 

A machine learning project for Realestate.co predicting residence house selling price by listing date.<br />

Used 14K property data split into 0.8 and 0.2 train, test. <br />

Achievements:<br />
New Zealand wide residential property selling price prediction by listing date<br />

$30K MAE(Mean Absolute Error) pridicting $100K to $400K range residential properties. <br />
$40K MAE when pridicting $400K to $1M price range residential properties.<br />
$50K MAE when pridicting $1M to $20M price range residential properties.<br /><br />

